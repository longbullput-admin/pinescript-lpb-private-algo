from enum import Enum
import inquirer
import geocoder
from TechnicalAnalysis import trailing_stop

questionnaire = [
    inquirer.List('trading_interval',
                  message="What timeframe do you want to trade on?",
                  choices=['1m', '3m', '5m', '15m', '30m'], carousel=True),

    inquirer.List('token',
                  message="What token do you want to trade on?",
                  choices=['ALGO', 'ETH', 'SOL', 'BTC'], carousel=True)
]


def trading_information(answers):
    """
        Goes through the answeres from the user and sets
        corresponding values to each element.
    """

    class tokens(Enum):
        ALGO = 'ALGO'
        ETH = 'ETH'
        SOL = 'SOL'
        BTC = 'BTC'

    class klines(Enum):
        KLINE_INTERVAL_1MINUTE = '1m'
        KLINE_INTERVAL_3MINUTE = '3m'
        KLINE_INTERVAL_5MINUTE = '5m'
        KLINE_INTERVAL_15MINUTE = '15m'
        KLINE_INTERVAL_30MINUTE = '30m'

    TRADE_SYMBOL_OUT = tokens(answers.get('token')).value

    TRADING_INTERVAL_OUT = klines(answers.get('trading_interval')).name

    TV_IV_VALUE_OUT = klines(answers.get('trading_interval')).value

    location = geocoder.ip('me')

    def loca(location):
        if location.country == "US":
            currency_out = "USD"
            international_code_out = "us"
            trading_pair_out = TRADE_SYMBOL_OUT + currency_out
            return trading_pair_out, international_code_out, currency_out

        if location.country != "US":
            currency_out = "USDT"
            international_code_out = "com"
            trading_pair_out = TRADE_SYMBOL_OUT + currency_out
            return trading_pair_out, international_code_out, currency_out


    trading_pair_out, international_code_out, currency_out,  = loca(location)

    return currency_out, TRADING_INTERVAL_OUT, TV_IV_VALUE_OUT, international_code_out, TRADE_SYMBOL_OUT, trading_pair_out