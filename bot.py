# From algo v1
import datetime
import pprint
from datetime import datetime

import dask.array as da
import inquirer
import numpy as np
from binance import ThreadedWebsocketManager
from binance.client import Client
from binance.enums import *

import GeneralFunctions as gf
import TechnicalAnalysis as ta
import questionnaire as survey
from dynaconf import settings

answers = inquirer.prompt(survey.questionnaire)
"""
    Initiates the questionnaire from questionnaire.py where the user gets
    asked whether or not they are located in the US or not, and what timeframe they
    would like to trade on.
"""
currency, TRADING_INTERVAL, TV_IV_VALUE, international_code, TRADE_SYMBOL, trading_pair_out = survey.trading_information(
    answers)

def init_history():
    # TODO: Make this more general so it can also be used for "Daily" VWAP
    # TODO: Adjust retrieved historical candles based on window size, interval and time denotion (i.e. minute, hour, day)
    global opens, highs, lows, closes, volumes, trades_qty, timestamps
    client = Client(api_key=settings.API_KEY , api_secret=settings.API_SECRET, tld=international_code)
    klines = client.get_historical_klines(symbol=trading_pair_out, interval=TV_IV_VALUE,
                                          start_str=f"{int(settings.CANDLE_WINDOW_SIZE) * (int(TV_IV_VALUE.replace('m', '')))} minute ago UTC")
    [opens, highs, lows, closes, volumes, trades_qty, timestamps] = [np.zeros(settings.CANDLE_WINDOW_SIZE) for _ in range(7)]
    for i in range(len(klines)):
        opens[i] = klines[i][1]
        highs[i] = klines[i][2]
        lows[i] = klines[i][3]
        closes[i] = klines[i][4]
        volumes[i] = klines[i][5]
        trades_qty[i] = klines[i][8]
        timestamps[i] = klines[i][6]

    print('init')
    return client


def in_positions():
    """
        For us to check wheter we hold more than 20% of available funds in the symbol, if so it is determined
        as we are in position and sell logic will apply.
    """
    global in_position
    client = Client(api_key=settings.API_KEY, api_secret=settings.API_SECRET, tld=international_code)
    asset_balance = float(client.get_asset_balance(asset=TRADE_SYMBOL).get('free'))
    available_fund = float(client.get_asset_balance(asset=currency).get('free'))
    if asset_balance >= 0.2 * available_fund:
        print("You are in position, awaiting sell signal")
        in_position = True
    else:
        print("You are not in position, awaiting buy signal")

def main(client):
    twm = ThreadedWebsocketManager(api_key=settings.API_KEY, api_secret=settings.API_SECRET, tld=international_code)
    twm.start()

    print('Opened connection to the socket')
    print("")

    def long_order(side, quantity, symbol, order_type=ORDER_TYPE_MARKET):
        try:
            print("sending long order")
            long_order = client.create_order(symbol=symbol, side=side, type=order_type, quantity=quantity)
            print(long_order)
        except Exception as e:
            print("an exception occured - {}".format(e))
            return False
        return True

    def on_message(message):
        global timestamps, closes, in_position, lows, highs, opens, volumes, trades_qty, in_position
        candle = message['k']
        timestamp = candle['T']
        ticker = candle['s']
        is_candle_closed = candle['x']
        open = candle['o']
        volume = candle['v']
        num_of_trades = candle['n']
        low = candle['l']
        high = candle['h']
        close = candle['c']

        if is_candle_closed:
            now = datetime.now()
            current_time = now.strftime("%H:%M:%S")
            print("Current Time =", current_time)

            pprint.pprint("candle closed at {}".format(close))
            timestamps = np.append(timestamps, float(timestamp))[1:]
            closes = np.append(closes, float(close))[1:]
            lows = np.append(lows, float(low))[1:]
            highs = np.append(highs, (float(high)))[1:]
            opens = np.append(opens, (float(open)))[1:]
            volumes = np.append(volumes, float(volume))[1:]
            trades_qty = np.append(trades_qty, (float(num_of_trades)))[1:]
            dda = da.from_array([closes, lows, highs, opens, volumes, trades_qty])
            ddacloses = dda[0].astype('float64').compute()
                
            """
                Fetching data from each candle to list for further work.
            """

            # Compute HP Trend
            trend = ta.trend_hp(closes)

            # Seperate variables, same thing. Turn to object or class when OOP.
            vwaps_closes = ta.vwap(closes, volumes, highs, lows, settings.VWAP_WINDOW)

            # Compute Lower and Upper bands for Trend HP filter
            # Commented out since we dont use ut.
            """
            lower_band_hp = ta.banding(trend, upper=False)
            upper_band_hp = ta.banding(trend)
            """

            # Compute OPB-PVT (Not the delta variant), using 1 day VWAP
            price_vol_trend = ta.price_volume_trend(vwaps_closes, volumes, settings.OBV_PVT_WINDOW)

            # OBV
            OBV_WINDOW = settings.OBV_PVT_WINDOW
            obv = ta.obv(np.diff(vwaps_closes), volumes, OBV_WINDOW)
            results = [m - n for m, n in zip(obv, price_vol_trend)]
            longbull = results[-1] > results[-2] and vwaps_closes[-1] < vwaps_closes[-2]

            # Compute inner candle
            inner_candle = ta.inner_candle(dda[1][-1], dda[1][-2], dda[2][-1], dda[2][-2])

            # Compute Strong Bear / Bull movement
            uberbull, uberbear = ta.strong_bear_bull(trend, settings.BEAR_BULL_WINDOW_SIZE)

            # Compute BB%B
            bbr, longbbr, sellbbr, upper, lower = ta.bbpb(vwaps_closes, settings.BBPB_PERIOD, settings.STDEVMULTIPLIER, settings.BBPB_OVERSOLD, settings.BBPB_OVERBOUGHT)

            # Compute OBV - PVT BB%B

            bbr2, longbbr2, sellbbr2, upper2, lower2 = ta.bbpb(results, settings.BBPB_PERIOD, settings.STDEVMULTIPLIER, settings.BBPB_OVERSOLD, settings.BBPB_OVERBOUGHT)

            # Compute Monter Breakout
            bbh, up, dn = ta.monster_breakout(opens=opens, highs=highs, lows=lows, closes=closes, n=settings.MONSTER_BREAKOUT_PERIOD)
            longcondition = opens[-1] < highs[-2] and lows[-1] < closes[-2]
            last_high_below_vwap = highs[-1] < trend[-1]
            crossover = closes[-1] > upper2 and closes[-2] < upper2

            # Buy and sell (doing client once more, because that way it does work)
            client = Client(api_key=settings.API_KEY, api_secret=settings.API_SECRET, tld=international_code)

            #Long condition
            long_condition =  inner_candle or longcondition or bbh or up and longbbr and not sellbbr and uberbull or last_high_below_vwap or longbull

            #Symbol information
            digits_computed, min_qty = ta.symbol_information(trading_pair_out, settings.API_KEY, settings.API_SECRET, international_code)

            # Long condition
            if in_position:
                in_position_closes = np.append(settings.IN_POSITION_CLOSES, closes[-1])
                in_position_closes = in_position_closes[in_position_closes != None]
                percent_diff, direction = ta.trade_history(closes, settings.API_KEY, settings.API_SECRET, international_code, trading_pair_out)

                print("In position, currently "f"{direction}"f" {percent_diff:.3%}")

                if sellbbr and (crossover or (trend[-1] > trend[-2] and closes[-1] > vwaps_closes[-1])):
                    # Insert binance long sell logic
                    ASSET_TO_SELL = client.get_asset_balance(asset=TRADE_SYMBOL)
                    INVENTORY = float(ASSET_TO_SELL.get('free'))

                    if digits_computed is None:
                        SELL_QUANTITY = ta.round_decimals_down(INVENTORY, decimals=0)
                    else: SELL_QUANTITY = ta.round_decimals_down(INVENTORY, decimals = digits_computed)

                    order_succeeded = long_order(SIDE_SELL, SELL_QUANTITY, trading_pair_out)
                    if order_succeeded:
                        gf.add_order_info_csv([closes, lows, highs, opens, volumes, trades_qty, trend, vwaps_closes,
                                               price_vol_trend, obv, results, longbull, inner_candle, uberbull,
                                               uberbear, bbr,
                                               longbbr, sellbbr, upper, lower, bbr2, longbbr2, sellbbr2, upper2, lower2,
                                               bbh, up,
                                               dn, longcondition, last_high_below_vwap, crossover, long_condition])

                        in_position = False
                        print("Long positions successfully closed, you do not hold any long positions")

            if not in_position:
                in_position_closes = None

                if inner_candle or longcondition or bbh or up:
                    if longbbr and not sellbbr:
                        if uberbull or last_high_below_vwap or longbull:

                            # Insert binance long buy logic
                            ASSET_BALANCE = client.get_asset_balance(asset=currency)
                            BUYING_POWER = float(ASSET_BALANCE.get('free'))*settings.PERCENTAGE_OF_BALANCE_TO_TRADE
                            BUY_QUANTITY = round(BUYING_POWER / float(closes[-1]), digits_computed)

                            if digits_computed is None:
                                BUY_QUANTITY = ta.round_decimals_down(BUY_QUANTITY, decimals=0)
                            else: BUY_QUANTITY = ta.round_decimals_down(BUY_QUANTITY, decimals = digits_computed)

                            if float(min_qty) <= BUY_QUANTITY:
                                order_succeeded = long_order(SIDE_BUY, BUY_QUANTITY, trading_pair_out)
                                if order_succeeded:
                                    gf.add_order_info_csv(
                                        [closes, lows, highs, opens, volumes, trades_qty, trend, vwaps_closes,
                                         price_vol_trend, obv, results, longbull, inner_candle, uberbull, uberbear, bbr,
                                         longbbr, sellbbr, upper, lower, bbr2, longbbr2, sellbbr2, upper2, lower2, bbh,
                                         up,
                                         dn, longcondition, last_high_below_vwap, crossover, long_condition])

                                    in_position = True
                                    print("Order successfully placed, you are now holding" f" {BUY_QUANTITY} positions")
                            else: 
                                print(
                                    "Insufficient funds to buy - please change symbol or add funds to your wallet"
                                )

            if not in_position:
                print("You currently hold no long positions")        

            print("")

    twm.start_kline_socket(
        callback=on_message, symbol=trading_pair_out, interval=TV_IV_VALUE)
    twm.join()


if __name__ == "__main__":
    client = init_history()
    in_positions()
    main(client=client)