import pandas as pd
import os


def add_order_info_csv(order_info):
    print(order_info)
    filename = 'Orderinfo.csv'
    path = os.getcwd()
    df = pd.DataFrame([order_info], columns=['closes', 'lows', 'highs', 'opens', 'volumes', 'trades_qty', 'trend',
                                             'vwaps_closes', 'price_vol_trend', 'obv', 'results', 'longbull',
                                             'inner_candle', 'uberbull', 'uberbear', 'bbr', 'longbbr', 'sellbbr',
                                             'upper', 'lower', 'bbr2', 'longbbr2', 'sellbbr2', 'upper2', 'lower2',
                                             'bbh', 'up', 'dn', 'longcondition', 'last_high_below_vwap', 'crossover',
                                             'long_condition'])
    if os.path.exists(path + '//Orderinfo.csv'):
        print("CSV Does exist")
        df.to_csv(path + '//Orderinfo.csv', mode='a', header=False, sep=';', index=False)
        pass
    else:
        print("CSV does not exist")
        df.to_csv(path + '//Orderinfo.csv', sep=';', index=False)
