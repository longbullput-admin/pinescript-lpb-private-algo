## LPB Private Algo
*Authors: LongBullPut Bot Team*

#
## 🥵 __Indicators Completed__
* VWAP HP replaced by trend
* VWAP daily and intervals
* PVT should be a go (awaiting final confirmation)
* OBV should be a go (awaiting final confirmation)
#
## ✨ __Features__
* Currently running on one symbol = ALGOUSDT
#
## 📃 __Prerequisites__
#### If running locally on Windows:
- Download [python](https://www.python.org/downloads/) if not already installed
- Download [pip](https://www.liquidweb.com/kb/install-pip-windows/) if not already installed
- Then run the following commands in a terminal
- !! Todo: need to verify this procedure and also is this ta_lib wheel for windows only? !!
    ```bash
    $ git clone git clone https://YOUR_USERNAME@bitbucket.org/longbullput/pinescript-lpb-private-algo.git
    $ cd pinescript-lpb-private-algo/bot
    $ pip install vendor/TA_Lib-0.4.21-cp39-cp39-win_amd64.whl
    $ pip install -r requirements.txt
    $ cd src
    $ python bot.py
    ```

#### If running in docker:
- Download [docker](https://docs.docker.com/get-docker/) if not already installed
- Then run the following commands in a terminal:
  ```bash
  $ git clone git clone https://YOUR_USERNAME@bitbucket.org/longbullput/pinescript-lpb-private-algo.git
  $ cd pinescript-lpb-private-algo
  $ docker-compose build
  $ docker-compose up -d
  ```
__Note__: When cloning the git repository, replace YOUR_USERNAME with your bitbucket username.<br />
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; You must have access to the repository in order to download.

#
## 😳 __Latest change to the script__
* Added buy sell logic, not sure if correct  - but it works.
* When adding said logic, we missed some minor parts in our output from our indicators. Adressed them and added.
* Still, not gone through each indicator and seen if they return the expected values. That is the next step. When that is all said and done, we can clean, debug, optimize and connect to our accounts to start trading.
#
## 🧠 __Indicators__
The following indicatiors are used;

* VWAP Daily interval (Based on previous days close)

* VWAP with HP filter

* VWAP Close (Based on trading interval)

* OBV - PVT (Based on VWAP Daily and Volume Daily)

* Inner Candles (Calculation on the last two candles, to check if previous candle is inside the candle before)

* Strong Bear / Bull Movement

* BB%B

* Monster Breakout


#
## ✨ __Whats left__
We need to check if every indicator returns expected output. This goes especially for the ones using VWAP, but also the running windows, slicing and computations. This is probably one of the heftier tasks that we have, but might be the most important one. The buy/sell logic is in, and with that I assume the strategies. However, we need to look into this in more depth.


#
## 💡 __Project folders__
* __[SRC]__

    * __Bot__.py (Consist of the bot, fetching klines and pulling the data through above indicators)
    * __config__.py (API key, and API )
    * __TechinalAnalysis__.py (As of now, all our functions are here)


#
## 🕵 __To Do's in the files__
### __1. Bot__.py
* Move static Global variables out, configurable into json/csv/else ❗
* Fill these with historical values based on selected interval ✔️
* Make this more general so it can also be used for "Daily" VWAP ✔️
* Adjust retrieved historical candles based on window size, interval and time denotion (i.e. minute, hour, day) ❗
### __2. Config__.py
* None
### __3. TechinalAnalysis__.py
* Get daily VWAP ✔️
* Figure out what lambda we need ❗

