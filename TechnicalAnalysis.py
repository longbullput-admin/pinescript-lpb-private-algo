import numpy as np
import statsmodels.api as sm
import math
import numpy as np
import talib
import dask.array as da
from binance.client import Client
from dynaconf import settings

API_KEY = settings.API_KEY 
API_SECRET = settings.API_SECRET

def banding(values, std_multiplier: int = 2, upper: bool = True):
    """
        Banding finds the upper or lower values from values using
        math.sqrt and numpy variance. It will based on user input decide to get the upper
        or lower band.
    """
    variance = np.var(values)  # Compute variance
    std = math.sqrt(variance)  # Get the sqrt
    sign = 1 if upper else -1
    return values + (sign * std) * std_multiplier


def vwap(prices, volumes, highs, lows, period):
    """
        VWAP uses prices and volumes to get the volume weighed average price. Here it checks wheter or not the length of
        prices equals the length of volumes. If so it uses numpy cumsum volum * prices divided by the volume
    """
    prices = prices[-period:]
    volumes = volumes[-period:]
    highs = highs[-period:]
    lows = lows[-period:]
    if len(prices) != len(volumes):
        print("Length of prices need to be equal to length of volumes")
        return -1.0
    if len(prices) <= 3:
        print("Length of prices need to be at least 3")
        return -1.0
    return np.cumsum(volumes * (highs + lows) / 2) / np.cumsum(volumes)


def trend_hp(prices, lamb: int = 1000):
    """
        The Hodrick-Prescott smoothing parameter. A value of 1600 is suggested for quarterly data.
        Ravn and Uhlig suggest using a value of 6.25 (1600/4**4) for annual data and 129600 (1600*3**4) for monthly data.
    """
    cycle, trend = sm.tsa.filters.hpfilter(prices, lamb=lamb)  # calculate the trend for HP
    return trend  # calculate trend using HP

def price_volume_trend(prices, volumes, period):
    """
        The Price Volume Trend (PVT) study attempts to quantify the amount of volume flowing into or out 
        of an instrument by identifying the close of the period in relation to the previous period�s close. 
        The volume for the period is then allocated accordingly to a running continuous total.

    """
    prices = prices[-period:]
    volumes = volumes[-period:]
    pv_list = []
    pvt_list = []
    for i in range(len(prices)):
        if i > 0:
            previous_close = prices[i]
            yesterdays_close = prices[i-1]
            previous_volume = volumes[i]
            pv = (((previous_close-yesterdays_close)/yesterdays_close)*previous_volume)
            pv_list.append(pv)

    for i in range(len(pv_list)):
        pvt = sum(pv_list[:i])
        pvt_list.append(pvt)
    
    return pvt_list


def obv(closes, volumes, period):
    """
        On Balance Volume (OBV) maintains a cumulative running total of the amount of volume occurring on up periods 
        compared to down periods.

        OBV = Cumulative (Up Volume - Down Volume)
    """
    #On Balance Volume (OBV)
    obv_list = []
    closes = closes[-period:]
    volumes = volumes[(-period)+1:]
    change_src = np.diff(closes)
    obv = talib.OBV(change_src, volumes)

    return obv


def inner_candle(low: float, prev_low: float, high: float, prev_high: float) -> bool:
    """
        An inside candle occurs when the candlestick of one candle's high and low falls within the 
        boundaries of the prior candles highs and lows. Inside candles can be indicative of indecision 
        in the market for a security, showing little price movement relative to the previous trading candle.
    """
    return (low > prev_low) and (high < prev_high)


def monster_breakout(opens, highs, lows, closes, n):
    """
        Find definition and document whatever we do here.
    """
    opens, highs, lows, closes = opens[-n:], highs[-n:], lows[-n:], closes[-n:]
    # hl2
    hl2 = (highs + lows) / 2
    # previous H and L
    hi = max(hl2)
    lo = min(hl2)
    # if current high is higher than hi (previous H), and low is lower then lo (previous L)
    up = highs[-1] > hi
    dn = lows[-1] < lo
    # the lowest close in 10 period back
    lowerbbhx = talib.MIN(closes, timeperiod=10)
    lowerbbh = lowerbbhx[-1]
    # determine the bbh
    bbh = False
    if lows[-1] == opens[-1] and opens[-1] < lowerbbh and opens[-1] < closes[-1] and closes[-1] > hl2[-1] + lows[-1]:
            bbh = True
    return bbh, up, dn


def strong_bear_bull(vwaps_hp, window_size):
    """
        Find definition and document whatever we do here.
    """
    change_vwap = np.diff(vwaps_hp)

    ma2 = talib.SMA(change_vwap, window_size)

    uberbull = (vwaps_hp[-1] > vwaps_hp[-2])
    uberbear = (change_vwap[-1] < ma2[-1])

    return uberbull, uberbear


def bbpb(daily_WVAP, period, STDEVMULTIPLIER, BBPB_OVERSOLD, BBPB_OVERBOUGHT):
    """
        Find definition and document whatever we do here.
    """
    # TODO: Get daily VWAP
    basis = sum(daily_WVAP[-period:]) / period
    dev = STDEVMULTIPLIER * np.std(daily_WVAP[-period:])
    upper = basis + dev
    lower = basis - dev
    bbr = (daily_WVAP[-1] - lower) / (upper - lower) #changed daily_WVAP[0]

    longbbr = (bbr <= BBPB_OVERSOLD)
    sellbbr = (bbr >= BBPB_OVERBOUGHT)

    return bbr, longbbr, sellbbr, upper, lower


def trade_history(closes, API_KEY, API_SECRET, international_code, trading_pair_out):
    client = Client(api_key=API_KEY, api_secret=API_SECRET, tld=international_code)
    trades = client.get_my_trades(symbol=trading_pair_out)
    
    longs_quantity = trades[-1].get('qty')

    longs_cost = trades[-1].get('price')

    initial_trade_value = float(longs_cost)*float(longs_quantity)
    current_trade_value = float(closes[-1])*float(longs_quantity)
    
    percent_diff = (current_trade_value - initial_trade_value) / initial_trade_value
    
    if percent_diff > 0: 
        direction = "up" 
    else: direction = "down"
    
    return percent_diff, direction

def trailing_stop(highest_close, TRAILING_STOP, closes):
    """
        Returns a value "stop" which is 2 percent lower than highest high.
    """
    stop = (highest_close - (highest_close*TRAILING_STOP))
    last_close = closes[-1]
    return stop, last_close

def symbol_information(trading_pair_out, API_KEY, API_SECRET, international_code):
    """
        Get information regarding the trading pair we are trading, to use for sell/buy to make sure we are compliant to
        Binance for min/max quantity, amount, and stepsize.
    """
    client = Client(api_key=API_KEY, api_secret=API_SECRET, tld=international_code)
    symbol_info = client.get_symbol_info(trading_pair_out)
    symbol_info_filtered = symbol_info['filters']
    stepsize = symbol_info_filtered[-6].get('stepSize') #Need a more robust way of this
    min_qty = symbol_info_filtered[-6].get('minQty')

    digits = (str(stepsize.index("1")))

    if digits == "0":
        digits_computed = None
    else:
        digits_computed = int(digits)-1
    return digits_computed, min_qty

def round_decimals_down(number:float, decimals:int=2):
    """
    Returns a value rounded down to a specific number of decimal places.
    """
    if not isinstance(decimals, int):
        raise TypeError("decimal places must be an integer")
    elif decimals < 0:
        raise ValueError("decimal places has to be 0 or more")
    elif decimals == 0:
        return math.floor(number)

    factor = 10 ** decimals
    return math.floor(number * factor) / factor