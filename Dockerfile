# Multi-stage build using virtualenv to reduce overhead from build and pip installs
# All packages are installed to /opt/venv

# Using slim because alpine is slow as fuck to build see --> https://pythonspeed.com/articles/alpine-docker-python/
FROM python:3.9.0-slim AS compile-image
RUN apt-get update
RUN apt-get install -y --no-install-recommends build-essential gcc wget

# Make sure we use the virtualenv:
RUN python -m venv /opt/venv
ENV PATH="/opt/venv/bin:$PATH"

# Upgrade pip
RUN python -m pip install --upgrade pip

# Numpy is a dependency we need first
RUN pip install numpy

# Download and build TA-Lib from source // Docs --> https://ta-lib.org/
RUN wget http://prdownloads.sourceforge.net/ta-lib/ta-lib-0.4.0-src.tar.gz && \
  tar -xvzf ta-lib-0.4.0-src.tar.gz && \
  cd ta-lib/ && \
  ./configure --prefix=/opt/venv && \
  make && \
  make install

# Install TA-Lib python wrapper
RUN pip install --global-option=build_ext --global-option="-L/opt/venv/lib" TA-Lib
RUN rm -R ta-lib ta-lib-0.4.0-src.tar.gz

# Install python requirements
COPY requirements.txt .
RUN pip install -r requirements.txt

# Finish build
FROM python:3.9.0-slim AS build-image
COPY --from=compile-image /opt/venv /opt/venv

# Make sure we use the virtualenv:
ENV PATH="/opt/venv/bin:$PATH"
ENV LD_LIBRARY_PATH="/opt/venv/lib"

# Copy all source files in local directory to /algobot directory in container
COPY . /algobot

# Finally, we can run the app
# Adding the -u flag makes it use unbuffered output for both stderr and stdout so we can see print() stuff
CMD ["python", "-u", "algobot/bot.py"]