import datetime
import math

import backtrader as bt

LOWLEN = 10


class BBPBStrategy(bt.Strategy):

    def __init__(self):
        self.bbpb = bt.indicators.BollingerBandsPct().pctb
        self.datalow = self.datas[0].low
        self.datahigh = self.datas[0].high
        self.dataclose = self.datas[0].close
        self.dataopen = self.datas[0].open
        self.the_highest_close_10 = bt.ind.Highest(self.data.close, period=LOWLEN)
        self.the_lowest_close_10 = bt.ind.Lowest(self.data.close, period=LOWLEN)

    def next(self):
        amount_to_invest = self.broker.cash
        self.size = math.floor(amount_to_invest / self.data.close)

        def bullBelt():
            if self.datalow[0] == self.dataopen[0]:
                return True
            if self.dataopen[0] < self.the_highest_close_10[0]:
                return True
            if self.dataopen[0] < self.dataclose[0]:
                if self.dataclose[0] > ((self.datahigh[0] - self.datalow[0]) / 2) + self.datalow[0]:
                    return True
            else:
                return False

        def bearBelt():
            if self.datahigh[0] == self.dataopen[0]:
                return True
            if self.dataopen[0] > self.the_lowest_close_10[0]:
                return True
            if self.dataopen[0] > self.dataclose[0]:
                if self.dataclose[0] < ((self.datahigh[0] - self.datalow[0]) / 2) + self.datalow[0]:
                    return True
            else:
                return False

        def icreturn():
            if self.datahigh[0] < self.datahigh[-1] and self.datalow[0] > self.datalow[-1]:
                return True
            else:
                return False

        if icreturn() or bullBelt():
            if self.bbpb[0] < 0 and not self.position:
                # BUY, BUY, BUY!!! (with all possible default parameters)
                self.order = self.buy_bracket(size=self.size, stopprice=self.dataclose - (self.dataclose * .5),
                                              limitprice=self.dataclose + (self.dataclose * .15))

        if icreturn() or bearBelt():
            if self.bbpb[0] > 1 and self.position:
                self.close()

        if icreturn() or bearBelt():
            if self.bbpb[0] > 1 and not self.position:
                # BUY, BUY, BUY!!! (with all possible default parameters)
                self.order = self.sell_bracket(size=self.size, stopprice=self.dataclose + (self.dataclose * .5),
                                               limitprice=self.dataclose - (self.dataclose * .15))

        if icreturn() or bullBelt():
            if self.bbpb[0] < 0 and self.position:
                self.close()


cerebro = bt.Cerebro()

print('Starting Portfolio Value: %.2f' % cerebro.broker.getvalue())

fromdate = datetime.datetime.strptime('2021-01-01', '%Y-%m-%d')
todate = datetime.datetime.strptime('2021-09-30', '%Y-%m-%d')

data = bt.feeds.GenericCSVData(dataname='data/202109_5minuteALGO.csv', dtformat=2, compression=15,
                               timeframe=bt.TimeFrame.Minutes, fromdate=fromdate, todate=todate)

cerebro.adddata(data)

cerebro.addstrategy(BBPBStrategy)

cerebro.run()

print('Final Portfolio Value: %.2f' % cerebro.broker.getvalue())

cerebro.plot()
